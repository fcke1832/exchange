Python 3.6.8
Django==2.2.5


# Устанавливаем зависимости
pip install requirements.txt


# Coздаем БД (postgres) 'exchange'


# Накатываем миграции


# Загружаем фикстуры с данными по валютам(данные содержат названия 10 популярных криптовалют)
./manage.py loaddata exchange/fixtures/currencies.json


# Запускаем management command для получения данных по валютам за последние 10 дней
./manage.py parser
