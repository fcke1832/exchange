# Byte-compiled / optimized / DLL files
*__pycache__/
*.pyc

# IntelliJ IDEA
*.iml
.idea/

# SublimeText
*.sublime-*

# VSCode
.vscode/

# Vim
*.swp
*.swo

# Mac OS
.DS_Store

*.dump