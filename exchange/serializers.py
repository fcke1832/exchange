from rest_framework import serializers
from .models import Rate, Currency


class RateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rate
        fields = '__all__'


class CurrencySerializer(serializers.ModelSerializer):
    detail_rate_url = serializers.SerializerMethodField()

    class Meta:
        model = Currency
        fields = ['id', 'name', 'detail_rate_url']

    def get_detail_rate_url(self, obj):
        return obj.get_absolute_url
