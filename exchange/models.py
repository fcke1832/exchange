from django.db import models
from django.urls import reverse


class Currency(models.Model):
    '''
    Модель валюты
    '''
    name = models.CharField('Currency name', max_length=4)
    fullname = models.CharField('Full currency name', max_length=50, blank=True)

    def __str__(self):
        return self.name

    @property
    def get_absolute_url(self):
        return reverse('exchange:rate', args=[str(self.id)])


class Rate(models.Model):
    '''
    Модель курса валюты
    '''
    currency = models.ForeignKey(Currency, null=False, blank=False, on_delete=models.CASCADE, related_name='rate')
    date = models.DateField('Date', null=False, blank=False)
    rate = models.FloatField('Rate', default=0, null=False, blank=False)
    volume = models.FloatField('Volume', default=0, null=False, blank=False)

    def __str__(self):
        return '{} - {}'.format(str(self.date), self.currency.name)

    class Meta:
        verbose_name = 'Rate'
        verbose_name_plural = 'Rates'
        ordering = ['currency', '-date']
        unique_together = (('date', 'currency'),)
