from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist

from .models import Currency
from .serializers import CurrencySerializer, RateSerializer

# Функция для вычисления среднего значения
def mean_value(rates):
    return float(sum(rates)) / max(len(rates), 1)


class CurrencyViewSet(ModelViewSet):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer


class RateView(APIView):
    serializer_class = RateSerializer

    def get(self, *args, **kwargs):
        try:
            currency = Currency.objects.get(id=kwargs['currency_id'])
        except ObjectDoesNotExist:
            return Response({'error': 'Currency is not found'})

        rates_qs = currency.rate.order_by('-date')[:10]
        currency.rate.exclude(pk__in=rates_qs).delete()  # Удаляем старые объекты
        volumes_list = [rate.volume for rate in rates_qs]  # Список значений торгов за последние дни

        try:
            volume = mean_value(volumes_list)
        except TypeError:
            return Response({'error': 'Volume is not float'})


        try:
            rate = rates_qs[0].rate
        except IndexError:
            return Response({'error': 'Rate is not found'})

        return Response({
            'currency': currency.name,
            'volume': volume,
            'rate': rate
        })
