import requests
from datetime import datetime

from django.core.management.base import BaseCommand
from exchange.models import Currency
from exchange.serializers import RateSerializer


class Command(BaseCommand):
    help = 'Parse currencies rates by last 10 days'

    def handle(self, *args, **options):
        base_url = 'https://api-pub.bitfinex.com/v2/'
        candles_params = 'candles/trade'
        time_params = '1D'

        all_currencies = Currency.objects.all()

        for currency in all_currencies:
            request_url = '{}{}:{}:t{}USD/hist'.format(base_url, candles_params, time_params, currency.name)
            response = requests.get(request_url)
            if response.status_code != 200:
                continue
            data = []
            response_data = response.json()
            response_data_by_ten_days = response_data[:10]  # Берем статистику за последние 10 дней
            for day_stats in response_data_by_ten_days:
                day_data = {'currency': currency.id}
                try:
                    ms = day_stats[0]  # милисекунды
                except IndexError:
                    continue

                try:
                    day_data['date'] = datetime.fromtimestamp(ms/1000.0).date()  # дата
                except ValueError:
                    continue

                try:
                    day_data['rate'] = day_stats[2]  # курс
                except IndexError:
                    continue

                try:
                    day_data['volume'] = day_stats[5]  # объем торгов
                except IndexError:
                    continue

                data.append(day_data)

            serializer = RateSerializer(data=data, many=True)
            if serializer.is_valid():
                serializer.save()
            else:
                print('serializer error >>>>>>>> {}'.format(serializer.errors))







