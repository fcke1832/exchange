from django.urls import path

from .views import CurrencyViewSet, RateView

app_name = 'exchange'
urlpatterns = [
    path('', CurrencyViewSet.as_view({'get': 'list'}), name='currencies_list'),
    path('<int:currency_id>/rate/', RateView.as_view(), name='rate')
]
